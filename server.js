const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});


app.put('/update/:id',(req,res)=>{
    // let reqID=req.params.id;
    // let product=products.filter(product=>{
    //     return product.id == reqID;
    // })[0];
    
    // let index=products.indexOf(product);
  
    // let keys = Object.keys(req.body);
    
    // keys.forEach(key=>{
    //     product[key]=req.body[key];
    // });
    
    // products[index]=product;
    // res.json(products[index]);
     const id = req.params.id;
   let updated = false;
   products.forEach((todo) =>{
      if(todo.id == id){
          updated = true;
          todo.productName = req.body.productName;
          todo.price = req.body.price;
      } 
   });
   if(updated){
    res.status(200).send(`Todo ${id} updated!`);    
   } else {
       res.status(404).send(`Could not find resource with id ${id}`);
   }
   
});


app.delete('/delete', (request, response) => {
  
  let prodproductName = request.body['productName'];

  let product = products.filter(product => {
    return product.productName == prodproductName;
  })[0];

  const index = products.indexOf(product);

  products.splice(index, 1);

  response.json({ message: `Prod ${prodproductName} deleted.`});

});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});