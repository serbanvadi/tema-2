import React, { Component } from 'react';
import './App.css';

import {AddProducts} from './components/AddProducts'
import {ProductList} from './components/ProductList'

class App extends Component {
  constructor(props){
    super(props);
    this.state={};
    this.state.products=[];
  }
  
  onProductAdded=(product)=>{
    let products=this.state.products;
    products.push(product);
    this.setState({
      products:products
    });
  }
  

  componentWillMount(){
    const url = 'https://seminar4-tehnologii-web-serbanadi.c9users.io/get-all'
    fetch(url).then((res) => {
      return res.json();
    }).then((produse) =>{
      this.setState({
        products: produse
      })
    })
  }
  
  render(){
    return(
      <React.Fragment>
        <ProductList title="Product" source={this.state.products}/>
      </React.Fragment>  
      );
  }
}

export default App;
